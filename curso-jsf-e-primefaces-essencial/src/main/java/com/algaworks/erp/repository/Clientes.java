package com.algaworks.erp.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.algaworks.erp.model.Cliente;

public class Clientes implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Clientes() {

	}
	
	public Clientes(EntityManager manager) {
		this.manager = manager;
	}
	
	public List<Cliente> todas() {
		 return manager.createQuery("from Cliente", Cliente.class).getResultList();
	}
	
	public Cliente porId(Long id) {
		return manager.find(Cliente.class, id);
	}
	
	public Cliente guardar(Cliente cliente) {
		return manager.merge(cliente);
	}

	public void remover(Cliente cliente) {
		cliente = porId(cliente.getId());
		manager.remove(cliente);
	}
}
