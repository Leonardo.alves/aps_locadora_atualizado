package com.algaworks.erp.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.algaworks.erp.model.Jogo;
import com.algaworks.erp.repository.Jogos;
import com.algaworks.erp.util.Transacional;

public class CadastroJogoService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Jogos jogos;
	
	@Transacional
	public void salvar(Jogo jogo) {
		jogos.guardar(jogo);
	}
	
	@Transacional
	public void excluir(Jogo jogo) {
		jogos.remover(jogo);
	}

}