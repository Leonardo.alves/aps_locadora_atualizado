package com.algaworks.erp.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.algaworks.erp.model.Cliente;
import com.algaworks.erp.repository.Clientes;
import com.algaworks.erp.util.Transacional;

public class CadastroClienteService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Clientes clientes;
	
	@Transacional
	public void salvar(Cliente cliente) {
		System.out.println(cliente);
		clientes.guardar(cliente);
	}
	
	@Transacional
	public void excluir(Cliente cliente) {
		clientes.remover(cliente);
	}

}