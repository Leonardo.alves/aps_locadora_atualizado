package com.algaworks.erp.controller;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.algaworks.erp.model.Jogo;
import com.algaworks.erp.repository.Jogos;
import com.algaworks.erp.service.CadastroJogoService;
import com.algaworks.erp.util.FacesMessages;

@Named("gestaoJogosBean")
@ViewScoped
public class GestaoJogosBean implements Serializable {

private static final long serialVersionUID = 1L;
	
	@Inject
	private Jogos jogos;
	
	@Inject
    private FacesMessages messages;
	
	@Inject
	private CadastroJogoService cadastroJogoService;
	
	private List<Jogo> listaJogos;
	
	@Inject
	private Jogo jogo;
	
	public Jogo getJogo() {
		return jogo;
	}
	
	public void setJogo(Jogo jogo) {
		this.jogo = jogo;
	}
	
	public void prepararNovoJogo() {
		jogo = new Jogo();
	}
	
	public void prepararEdicaoJogo() {
		jogo = new Jogo();
	}
	
	public void salvar() {
//		jogo.setTitulo("leandro");
//		jogo.setClassificacaoIndicativa("18");
//		jogo.setDesenvolvedor("SONY");
//		jogo.setGenero("Luta");
//		jogo.setPlataforma("PS3");
		
		cadastroJogoService.salvar(jogo);
		messages.info("Título salvo com sucesso!");
		RequestContext.getCurrentInstance().update(Arrays.asList(
			"frm:jogosDataTable", 
			"frm:messages"
		));
		todosJogos();
	}

	public void excluir() {
		cadastroJogoService.excluir(jogo);
		jogo = null;
		todosJogos();
		messages.info("Título deletado com sucesso!");
	}
	
	public void todosJogos() {
		listaJogos = jogos.todas();
	}
	
	public List<Jogo> getListaJogos() {
		List<Jogo> listaJogos = jogos.todas();
		return listaJogos;
	}
		
	public boolean isJogoSeleciona() {
		return jogo != null && jogo.getId() != null;
	}

}
